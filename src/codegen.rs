use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

use llvm::{BasicBlock, Builder, Compile, Context, Function, GetContext, FunctionType, Module, Type, Value};

use ast::{Decl, Expr, ExprBlockData, LangType, Op, Stmt, StmtBlockData, Program};

pub struct CompilerContext<'a> {
    pub context: &'a Context,
    pub module: &'a Module,
    pub builder: &'a Builder,
    pub sym_table: HashMap<String, &'a Value>,
    pub curr_block: &'a BasicBlock,
    pub curr_func: &'a Function,
}

impl ExprBlockData {
    pub fn codegen<'a>(&self, cc: Rc<RefCell<CompilerContext<'a>>>) -> &'a Value {
        let block = cc.borrow().curr_func.append("");
        cc.borrow().builder.position_at_end(*&block);

        for v in &self.stmts {
            v.codegen(cc.clone());
        }

        cc.borrow().builder.position_at_end(*&block);
        self.val.codegen(cc.clone())
    }
}

impl StmtBlockData {
    pub fn codegen<'a>(&self, cc: Rc<RefCell<CompilerContext<'a>>>) {
        let block = cc.borrow().curr_func.append("");
        cc.borrow().builder.position_at_end(*&block);

        for v in &self.stmts {
            v.codegen(cc.clone());
        }

        cc.borrow().builder.position_at_end(*&block);
    }
}

impl Expr {
    pub fn codegen<'a>(&self, cc: Rc<RefCell<CompilerContext<'a>>>) -> &'a Value {
        match *self {
        // Literals
            Expr::I32(ref v) => v.compile(cc.borrow().context),
            Expr::I64(ref v) => v.compile(cc.borrow().context),
            Expr::F32(ref v) => v.compile(cc.borrow().context),
            Expr::F64(ref v) => v.compile(cc.borrow().context),
            Expr::Str(ref v) => v.compile(cc.borrow().context),
        //
            Expr::BinOp { ref l, ref op, ref r } => {
                let L = l.codegen(cc.clone());
                let R = r.codegen(cc.clone());
                match *op {
                    Op::Add => cc.borrow().builder.build_add(&L, &R),
                    Op::Sub => cc.borrow().builder.build_sub(&L, &R),
                    Op::Mul => cc.borrow().builder.build_mul(&L, &R),
                    Op::Div => cc.borrow().builder.build_div(&L, &R),
                    // TODO: Other operators (^, |, &, >>, <<, **)
                }
            },
            Expr::FnCall { ref name, ref args } => {
                let mut args2: Vec<&Value> = Vec::new();
                for x in args {
                    args2.push(x.codegen(cc.clone()));
                }
                // TODO: Proper error messages (with line numbers)
                let func = cc.borrow().module.get_function(&name).expect(&format!("No function named `{}`\n", &name));
                let params = func.get_signature().get_params();
                assert_eq!(params.len(), args2.len(),
                    "Incorrect number of args passed to function `{}`: should be {}, received {}\n", &name, args2.len(), params.len());
                for i in 0..args.len() {
                    assert_eq!(args[i].get_type(cc.clone()), params[i],
                        "In calling function `{}`, type for arg {} doesn't match", &name, i);
                }
                cc.borrow().builder.build_call(func, &args2)
            },
            Expr::ExprBlock(ref d) => d.codegen(cc.clone()),
            Expr::Ident { ref name } => cc.borrow().sym_table.get(name).expect(&format!("Not sure what you mean by the symbol `{}` :(", &name)),

            _ => {
                println!("Unimplemented: {:?}\n", &self);
                unimplemented!()
            },
        }
    }
 
}

impl Stmt {
    fn def_fn<'a>(cc: Rc<RefCell<CompilerContext<'a>>>, name: &String, params: &Vec<Decl>,  expr_body: Option<&ExprBlockData>, stmt_body: Option<&StmtBlockData>, typ: Option<&LangType>) {
        if cc.borrow().module.get_function(&name).is_some() {
            panic!("Redefined function `{}`", name);
        }
        let ret_type = match typ {
            Some(t) => t.get_type(cc.clone()),
            None => Type::get::<()>(cc.borrow().context),
        };
        let arg_types: Vec<&Type> = params.iter().map(|x| &*x.typ.get_type(cc.clone())).collect();
        let func_type: &FunctionType = FunctionType::new(ret_type, arg_types.as_slice());
        let func: &'a Function = cc.borrow().module.add_function(name, func_type);
        cc.borrow_mut().curr_func = func;

        /*let mut prev: HashMap<String, &Value> = HashMap::new();
        
        for arg in 0..params.len() {
            let s = &params[arg].name;
            if let Some(old_val) = cc.borrow().sym_table.get(s) {
                if prev.contains_key(s) {
                    panic!("Duplicate argument name `{}` passed in to function `{}`", s, name);
                }
                prev.insert(s.clone(), old_val);
            }
            let t: &'a Value = &func[arg];
            cc.borrow_mut().sym_table.insert(s.to_string(), t);
        }*/

        let prev_func = cc.borrow().curr_func;
        cc.borrow_mut().curr_func = func;

        let block = cc.borrow().curr_block;

        match typ {
            Some(_) => {
                let val = expr_body.unwrap().codegen(cc.clone());
                cc.borrow().builder.build_ret(&val);
            },
            None => {
                stmt_body.unwrap().codegen(cc.clone());
                cc.borrow().builder.build_ret_void();
            }
        };
        cc.borrow_mut().curr_func = prev_func;
        cc.borrow().builder.position_at_end(&*block);
    }

    pub fn codegen<'a>(&self, cc: Rc<RefCell<CompilerContext>>) {
        match *self {
            Stmt::Expr(ref e) => { e.codegen(cc.clone()); },
            Stmt::FnDef {ref name, ref params, ref body, ref typ} => Stmt::def_fn(cc, name, params, Some(body), None, Some(typ)),
            Stmt::VoidFnDef {ref name, ref params, ref body} => Stmt::def_fn(cc, name, params, None, Some(body), None),
            Stmt::StmtBlock(ref d) => d.codegen(cc.clone()),
            _ => {
                println!("Unimplemented: {:?}\n", &self);
                unimplemented!()
            },
        };
    }
}

impl Program {
    pub fn codegen<'a>(&self, cc: Rc<RefCell<CompilerContext>>) {
        for v in &self.stmts {
            v.codegen(cc.clone());
        }
    }
}
