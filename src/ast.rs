use std::cell::RefCell;
use std::rc::Rc;
use llvm::{Compile, GetContext, Type};
use codegen::CompilerContext;

#[derive(Debug)]
pub enum Expr {

// Literals
    I32(i32),
    I64(i64),
    F32(f32),
    F64(f64),
    Str(String),
//
    None, // TODO: Make this tuple of length 0
    BinOp{l: Box<Expr>, op: Op, r: Box<Expr>},
    FnCall{name: String, args: Vec<Box<Expr>>},
    ExprBlock(ExprBlockData),
    ExprIf{blocks: Vec<(Box<Expr>, ExprBlockData)>},
    Reassign{name: String, expr: Box<Expr>},
    Ident{name: String},
}

impl Expr {
    pub fn get_type<'a>(&self, cc: Rc<RefCell<CompilerContext<'a>>>) -> &'a Type {
        match *self {
            Expr::I32(_) => i32::get_type(cc.borrow().module.get_context()),
            Expr::I64(_) => i64::get_type(cc.borrow().module.get_context()),
            Expr::F32(_) => f32::get_type(cc.borrow().module.get_context()),
            Expr::F64(_) => f64::get_type(cc.borrow().module.get_context()),
            Expr::Str(_) => <&'static str as Compile<'a>>::get_type(cc.borrow().module.get_context()), // FIXME: 'static
            Expr::None => <() as Compile<'a>>::get_type(cc.borrow().module.get_context()),
            Expr::BinOp{ref l, ..} => l.get_type(cc.clone()),               // TODO: Ensure that the type of l is equal to the type of r
            Expr::FnCall{ref name, ref args} => {
                let func = cc.borrow().module.get_function(name).expect("get_type FnCall func doesn't exist");
                func.get_signature().get_return()
            },
            Expr::ExprBlock(ref r) => r.val.get_type(cc.clone()),
            Expr::ExprIf{ref blocks} => blocks[0].1.val.get_type(cc.clone()),   // TODO: Ensure that the types of all entries match
            Expr::Reassign{ref expr, ..} => expr.get_type(cc.clone()),      // TODO: Ensure that the assignment types match + with Assign
            Expr::Ident{ref name} => {
                let CC = cc.borrow();
                let val = CC.sym_table.get(name).expect(&format!("get_type Ident `{}` doesn't exist", name));
                val.get_type()
            },
            _ => {
                println!("Unimplemented: {:?}\n", &self);
                unimplemented!()
            },
        }
    }
}




#[derive(Debug)]
pub enum LangType {
    I32,
    I64,
    F32,
    F64,
    Str,
}

impl LangType {
    pub fn get_type<'a>(&self, cc: Rc<RefCell<CompilerContext<'a>>>) -> &'a Type {
        match *self {
            LangType::I32 => i32::get_type(cc.borrow().module.get_context()),
            LangType::I64 => i64::get_type(cc.borrow().module.get_context()),
            LangType::F32 => f32::get_type(cc.borrow().module.get_context()),
            LangType::F64 => f64::get_type(cc.borrow().module.get_context()),
            LangType::Str => <&'static str as Compile<'a>>::get_type(cc.borrow().module.get_context()), // FIXME: 'static
            _ => {
                println!("Unimplemented: {:?}\n", &self);
                unimplemented!()
            },
        }
    }
}

#[derive(Debug)]
pub struct ExprBlockData {
    pub stmts: Vec<Box<Stmt>>,
    pub val: Box<Expr>
}

#[derive(Debug)]
pub enum Stmt {
    Expr(Box<Expr>),
    FnDef{name: String, params: Vec<Decl>, body: ExprBlockData, typ: LangType},
    VoidFnDef{name: String, params: Vec<Decl>, body: StmtBlockData},
    Assign{name: String, expr: Box<Expr>, typ: LangType},
    StmtBlock(StmtBlockData),
    StmtIf{blocks: Vec<(Box<Expr>, StmtBlockData)>}
}

#[derive(Debug)]
pub struct StmtBlockData {
    pub stmts: Vec<Box<Stmt>>
}


#[derive(Debug)]
pub enum Op {
    Add,
    Sub,
    Mul,
    Div
}

#[derive(Debug)]
pub struct Decl {
    pub typ: LangType,
    pub name: String
}

#[derive(Debug)]
pub struct Program{
    pub stmts: Vec<Box<Stmt>>,
}
