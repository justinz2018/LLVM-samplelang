#![feature(box_syntax)]
extern crate llvm;
extern crate llvm_sys;

mod lang;
mod ast;
mod codegen;

use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;
use llvm::{Builder, Context, Module, Type};
use codegen::{CompilerContext};

fn main() {
    let ct = Context::new();
    let m = Module::new("test", &ct.as_semi());
    let b = Builder::new(&ct.as_semi());

    let main_func = m.add_function("main", Type::get::<fn() -> ()>(&ct));
    let block = main_func.append("");
    b.position_at_end(*&block);

    let c = Rc::new(RefCell::new(CompilerContext { context: &ct, module: &m, builder: &b, sym_table: HashMap::new(),
            curr_func: main_func, curr_block: block }));

    println!("{:?}\n", lang::parse_Expr("22.5L").unwrap().codegen(c.clone()));
    println!("{:?}\n", lang::parse_Expr("\"a225L\"").unwrap());
    println!("{:?}\n", lang::parse_Expr("\"225L\"").unwrap().codegen(c.clone()));
    // println!("{:?}\n", lang::parse_Expr("\"a225L\"").unwrap().codegen(c.clone()));
    println!("{:?}\n", lang::parse_Expr("{ a; 123; 43 }").unwrap());
    println!("{:?}\n", lang::parse_Program(r#"def a(i32 x, i64 z) {} a(1, 1L+4L+5L+2L);"#).unwrap().codegen(c.clone()));
    // println!("{:?}\n", lang::parse_Stmt("if x { print(x); 2+5; }").unwrap().codegen(c.clone()));
    println!("{:?}\n", lang::parse_Expr("22L*44.1L+66").unwrap());
    println!("{:?}\n", lang::parse_Expr("LKQL*A342+66").unwrap());
    println!("{:?}\n", lang::parse_Expr("{ 1.2; 4L; 234; 543534; 5 }").unwrap());
    println!("{:?}\n", lang::parse_Stmt("j109j32sd_(1.2, 45L, 2);").unwrap());
    println!("{:?}\n", lang::parse_Stmt("def laksdf1(i32 a1, f64 ajsajO_) { 1; 2; }").unwrap());
    println!("{:?}\n", lang::parse_Stmt("def laksdf12(i32 a1, f64 basdjSDj3, i64 asjl2340901) -> i64 { 1; 2L }").unwrap().codegen(c.clone()));
    println!("{:?}\n", lang::parse_Expr("if 1 { 23.4; 4.5; 6.7; 3 } elif 2 { 2 } else { 5 }").unwrap());
    println!("{:?}\n", lang::parse_Stmt("i32 x = 5;").unwrap());
    println!("{:?}\n", lang::parse_Expr("x = y = 6").unwrap());
    println!("{:?}\n", lang::parse_Stmt("i64 x = y = 6;").unwrap());
    println!("{:?}\n", lang::parse_Stmt("y = 6;").unwrap());
    println!("{:?}\n", lang::parse_Expr("{ i32 x = 5; y = 6 }").unwrap());
    println!("{:?}\n", lang::parse_Program(
            /*  i32 y = 1+2*3;
                i64 x = 5L;
                i32 z = 5;
                z = y = 1;
                i64 w = x = 2L; */
        r#" def test() -> i32 {
                3.5L;
                2L; 4+6-3*4;
                "def this_should_not_work(i32 x, i65 y) -> i64 { 2L }";
                2+5
            }
            test();
            "#).unwrap().codegen(c.clone())
    );

    b.position_at_end(*&block);
    b.build_ret_void();

    m.verify().unwrap();

    unsafe {
        let ptr: llvm_sys::prelude::LLVMModuleRef = m.as_ptr();
        llvm_sys::core::LLVMDumpModule(ptr);
    }

}
